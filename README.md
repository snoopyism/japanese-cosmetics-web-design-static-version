# Japanese Cosmetics Webpage (Static Version)

## Introduction

This repository features a static version of a Japanese cosmetics webpage, designed using a [Figma mockup](https://www.figma.com/community/file/1123424832494855532/japanese-cosmetics-web-design) created by [Mayuko Soga](https://www.figma.com/@mayukosoga). The purpose of this repository is to facilitate the study and practice of fundamental HTML and CSS web development skills.